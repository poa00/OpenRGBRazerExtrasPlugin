#ifndef RAZEREXTRASMAINPAGE_H
#define RAZEREXTRASMAINPAGE_H

#include <QWidget>

namespace Ui {
class RazerExtrasMainPage;
}

class RazerExtrasMainPage : public QWidget
{
    Q_OBJECT

public:
    explicit RazerExtrasMainPage(QWidget *parent = nullptr);
    ~RazerExtrasMainPage();

private:
    Ui::RazerExtrasMainPage *ui;
};

#endif // RAZEREXTRASMAINPAGE_H
