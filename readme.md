# Razer extras Plugin 

[![pipeline status](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/badges/master/pipeline.svg)](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/commits/master)

## What is this?

Adds extra functions to razer devices.

Currently suppported:

- Resets Chroma ARGB USB Controller

## Experimental (Master)

* [Windows 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/artifacts/master/download?job=Windows%2032)
* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/artifacts/master/download?job=Windows%2064)
* [Linux 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/artifacts/master/download?job=Linux%2032)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/artifacts/master/download?job=Linux%2064)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/artifacts/master/download?job=MacOS%20ARM64)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/artifacts/master/download?job=MacOS%20Intel)

## Stable (0.8)

* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/3418208290/artifacts/download)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/3418208288/artifacts/download)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/3418208291/artifacts/download)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin/-/jobs/3418208292/artifacts/download)

## How do I install it?

* Download and extract the correct files depending on your system
* Launch OpenRGB
* From the Settings -> Plugins menu, click the "Install plugin" button

