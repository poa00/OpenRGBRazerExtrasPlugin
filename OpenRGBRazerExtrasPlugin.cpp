#include "OpenRGBRazerExtrasPlugin.h"
#include <QHBoxLayout>
#include "RazerExtrasMainPage.h"

bool OpenRGBRazerExtrasPlugin::DarkTheme = false;
ResourceManager* OpenRGBRazerExtrasPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBRazerExtrasPlugin::GetPluginInfo()
{
    printf("[OpenRGBRazerExtrasPlugin] Loading plugin info.\n");

    OpenRGBPluginInfo info;

    info.Name           = "Razer extras plugin";
    info.Description    = "Plugin that allow you to control certain features on Razer devices";
    info.Version        = VERSION_STRING;
    info.Commit         = GIT_COMMIT_ID;
    info.URL            = "https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin";

    info.Icon.load(":/OpenRGBRazerExtrasPlugin.png");

    info.Location       =  OPENRGB_PLUGIN_LOCATION_TOP;
    info.Label          =  "Razer extras";
    info.TabIconString  =  "Razer extras";

    info.TabIcon.load(":/OpenRGBRazerExtrasPlugin.png");

    return info;
}

unsigned int OpenRGBRazerExtrasPlugin::GetPluginAPIVersion()
{
    printf("[OpenRGBRazerExtrasPlugin] Loading plugin API version.\n");

    return OPENRGB_PLUGIN_API_VERSION;
}

void OpenRGBRazerExtrasPlugin::Load(bool dark_theme, ResourceManager* resource_manager_ptr)
{
    printf("[OpenRGBRazerExtrasPlugin] Loading plugin.\n");

    RMPointer                = resource_manager_ptr;
    DarkTheme                = dark_theme;
}

QWidget* OpenRGBRazerExtrasPlugin::GetWidget()
{
    printf("[OpenRGBRazerExtrasPlugin] Creating widget.\n");

    return new RazerExtrasMainPage(nullptr);
}

QMenu* OpenRGBRazerExtrasPlugin::GetTrayMenu()
{
    return nullptr;
}

void OpenRGBRazerExtrasPlugin::Unload()
{
    printf("[OpenRGBRazerExtrasPlugin] Time to call some cleaning stuff.\n");
}

OpenRGBRazerExtrasPlugin::OpenRGBRazerExtrasPlugin()
{
    printf("[OpenRGBRazerExtrasPlugin] Constructor.\n");
}

OpenRGBRazerExtrasPlugin::~OpenRGBRazerExtrasPlugin()
{
    printf("[OpenRGBRazerExtrasPlugin] Time to free some memory.\n");
}

